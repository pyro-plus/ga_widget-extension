<?php

return [
    'google_application_name'              => [
        'type'        => 'anomaly.field_type.text',
        'env'         => 'GOOGLE_APPLICATION_NAME',
        'bind'        => 'defr.extension.ga_widget::config.google_application_name',
        'config'      => [
            'default_value' => '',
        ],
    ],
    'google_developer_key'                 => [
        'type'        => 'anomaly.field_type.text',
        'env'         => 'GOOGLE_DEVELOPER_KEY',
        'bind'        => 'defr.extension.ga_widget::config.google_developer_key',
        'config'      => [
            'default_value' => '',
        ],
    ],
    'google_service_enabled'               => [
        'type'        => 'anomaly.field_type.boolean',
        'env'         => 'GOOGLE_SERVICE_ENABLED',
        'bind'        => 'defr.extension.ga_widget::config.google_service_enabled',
        'config'      => [
            'default_value' => false,
        ],
    ],
    'google_service_account_json_location' => [
        'type'        => 'anomaly.field_type.text',
        'env'         => 'GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION',
        'bind'        => 'defr.extension.ga_widget::config.google_service_account_json_location',
        'config'      => [
            'default_value' => '',
        ],
    ],
];
