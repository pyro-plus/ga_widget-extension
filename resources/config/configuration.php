<?php

use Carbon\Carbon;
use Defr\GaWidgetExtension\GaWidgetChartsRegistry;

return [
    // 'start_date' => [
    //     'type'   => 'anomaly.field_type.datetime',
    //     'config' => [
    //         'default_value' => Carbon::now()->subYear(),
    //     ],
    // ],
    // 'end_date'   => [
    //     'type'   => 'anomaly.field_type.datetime',
    //     'config' => [
    //         'default_value' => Carbon::now(),
    //     ],
    // ],
    'interval'   => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'default_value' => 'subWeek',
            'options'       => [
                'subWeek'  => 'Week',
                'subMonth' => 'Month',
            ],
        ],
    ],
    'view'       => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'mode'    => 'search',
            'options' => function ()
            {
                $client  = Google::getClient();
                $service = Google::make('analytics');
                $options = [];

                $summaries = $service->management_accountSummaries
                ->listManagementAccountSummaries()
                ->getItems()[0]
                ->getWebProperties();

                foreach ($summaries as $summary)
                {
                    foreach ($summary->getProfiles() as $profile)
                    {
                        $options[$profile->getId()] = $profile->getName().' '
                        .'('.$profile->getId().') '.$profile->getType().'  '
                        .$summary->getWebsiteUrl();
                    }
                }

                return $options;
            },
        ],
    ],
    'chart_type' => [
        'type'     => 'anomaly.field_type.select',
        'required' => true,
        'config'   => [
            'options' => [
                'line' => 'Line',
                'bar'  => 'Bar',
                'pie'  => 'Pie',
            ],
        ],
    ],
];
