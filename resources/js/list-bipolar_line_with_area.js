/*global Chartist*/
/*eslint-disable one-var*/
/*eslint-disable indent*/
+(function (Chartist, document) {
    var elements = document.querySelectorAll('.widget.card');

    elements.forEach(function (element) {
        var chart = element.querySelector('.ct-chart');

        if (chart) {
            var data = chart.dataset;
            new Chartist.Line(chart, {
                labels: JSON.parse(data.labels),
                series: JSON.parse(data.series),
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                },
                // high: chart.dataset.optHigh || 3,
                // low: chart.dataset.optLow || -3,
                // showArea: chart.dataset.optShowArea || true,
                // showLine: chart.dataset.optShowLine || false,
                // showPoint: chart.dataset.optShowPoint || false,
                // fullWidth: chart.dataset.optFixedWidth || true,
                // axisX: {
                //     showLabel: chart.dataset.optAxisXShowLabel || false,
                //     showGrid: chart.dataset.optAxisXShowGrid || false,
                // },
            });
        }
    });
})(Chartist, document);
