<?php

return [
    'title'       => 'Chart Widget',
    'name'        => 'Chart Widget Extension',
    'description' => 'A dashboard widget that displays an Google Analytics charts.',
];
