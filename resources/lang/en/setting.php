<?php

return [
    'google_application_name' => [
        'name' => 'Google Application Name',
    ],
    'google_developer_key' => [
        'name' => 'Google Developer Key',
    ],
    'google_service_enabled' => [
        'name' => 'Google Service Enabled',
    ],
    'google_service_account_json_location' => [
        'name' => 'Google Service Account Json Location',
    ],
];
