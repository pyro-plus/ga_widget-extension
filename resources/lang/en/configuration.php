<?php

return [
    'start_date' => [
        'name' => 'Start Date',
    ],
    'end_date' => [
        'name' => 'End Date',
    ],
    'method' => [
        'name' => 'Method',
    ],
    'chart_type' => [
        'name' => 'Chart Type',
    ],
];
