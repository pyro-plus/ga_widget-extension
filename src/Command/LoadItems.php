<?php namespace Defr\GaWidgetExtension\Command;

use Anomaly\ConfigurationModule\Configuration\Contract\ConfigurationRepositoryInterface;
use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Carbon\Carbon as Carbon;
use Defr\GaWidgetExtension\GaWidgetChartsRegistry;
use Google;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class LoadItems
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class LoadItems
{
    use DispatchesJobs;

    /**
     * The widget instance.
     *
     * @var WidgetInterface
     */
    protected $widget;

    /**
     * ID of google view.
     *
     * @var string
     */
    protected $viewId;

    /**
     * Showing interval.
     *
     * @var string
     */
    protected $interval;

    /**
     * Create a new LoadItems instance.
     *
     * @param WidgetInterface $widget
     */
    public function __construct(WidgetInterface $widget)
    {
        $this->widget = $widget;
    }

    /**
     * Handle the widget data.
     *
     * @param Repository                       $cache
     * @param ConfigurationRepositoryInterface $configuration The configuration
     */
    public function handle(
        Repository $cache,
        ConfigurationRepositoryInterface $configuration
    )
    {
        $this->viewId = $configuration->value(
            'defr.extension.ga_widget::view',
            $this->widget->getId()
        );

        $this->interval = $configuration->value(
            'defr.extension.ga_widget::interval',
            $this->widget->getId()
        );

        $report = $cache->remember(
            __METHOD__.'_'.$this->widget->getId(),
            30,
            function ()
            {
                $method    = $this->interval;
                $endTime   = Carbon::now();
                $startTime = $endTime->$method();

                $client = Google::getClient();

                $service   = Google::make('analyticsReporting');
                $requests  = Google::make('analyticsReporting_GetReportsRequest');
                $request   = Google::make('analyticsReporting_ReportRequest');
                $dateRange = Google::make('analyticsReporting_DateRange');

                $dateRange->setStartDate($startTime->format('Y-m-d'));
                $startDate = $dateRange;

                $dateRange->setEndDate($endTime->format('Y-m-d'));
                $endDate = $dateRange;

                $dateRanges = [$startDate, $endDate];

                $request->setViewId($this->viewId);
                $request->setDateRanges([$startDate, $endDate]);
                $requests->setReportRequests([$request]);

                $response = $service->reports->batchGet($requests);

                return $response->getReports()[0]; //[0]->getData();
            }
        );
        // dd($report);

        $headerEntries = array_map(
            function ($entry)
            {
                return $entry->getName();
            },
            $report->getColumnHeader()->getMetricHeader()->getMetricHeaderEntries()
        );

        $totals = $report->getData()->getTotals();
        $labels = [];
        $items  = [];
        $date   = Carbon::now()->addDay();

        foreach ($totals as $timeRange)
        {
            $labels[] = $date->subDay()->format('D');
            $items[]  = $timeRange->getValues()[0];
        }

        $this->widget->addData('labels', array_reverse($labels));
        $this->widget->addData('items', $items);
    }

    /**
     * Gets the dates from range.
     *
     * @param  string $date_time_from The date time from
     * @param  string $date_time_to   The date time to
     * @return array  The dates from range.
     */
    private function getDatesFromRange($date_time_from, $date_time_to)
    {
        $start = Carbon::createFromFormat('Y-m-d', substr($date_time_from, 0, 10));
        $end   = Carbon::createFromFormat('Y-m-d', substr($date_time_to, 0, 10));
        $dates = [];

        while ($start->lte($end))
        {
            $dates[] = $start->copy()->format('Y-m-d');
            $start->addDay();
        }

        return $dates;
    }
}
