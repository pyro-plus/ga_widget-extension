<?php namespace Defr\GaWidgetExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use PulkitJalan\Google\Facades\Google;
use PulkitJalan\Google\GoogleServiceProvider;

class GaWidgetExtensionServiceProvider extends AddonServiceProvider
{

    /**
     * Addon providers
     *
     * @var array
     */
    protected $providers = [
        GoogleServiceProvider::class,
    ];

    /**
     * Addon aliases
     *
     * @var array
     */
    protected $aliases = [
        'Google' => Google::class,
    ];
}
