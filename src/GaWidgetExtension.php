<?php namespace Defr\GaWidgetExtension;

use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Anomaly\DashboardModule\Widget\Extension\WidgetExtension;
use Defr\GaWidgetExtension\Command\LoadItems;

class GaWidgetExtension extends WidgetExtension
{

    /**
     * This extension provides
     *
     * @var null|string
     */
    protected $provides = 'anomaly.module.dashboard::widget.ga';

    /**
     * Load the widget data.
     *
     * @param WidgetInterface $widget
     */
    protected function load(WidgetInterface $widget)
    {
        $this->dispatch(new LoadItems($widget));
    }
}
